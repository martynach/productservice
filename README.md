# productservice - REST API
## productservice - 
It is a simple service implementing Mongo CRUD operations using Mongoose.
It also shows an example of more complicated queries based on aggregates.
Queries are implemented for products collection which embeds categories and suppliers.
You can try out some queries by going to appropriate endpoints. See swagger docs for more details.

Or you can use **ProductsUi** service which communicates with productservice and demonstrates some of it's possibilities.
### [ProductsUi](https://flamboyant-jennings-0000f7.netlify.com) deployed on Netlify
### [ProductsUi](https://gitlab.com/martynach/products-ui) repository



# Technologies used in the project
* Mongo
* Mongoose
* Node
* Tsoa and Inversify

Go to [wiki](https://gitlab.com/martynach/productservice/wikis/home) to see more details of each technology.



## Setting development environment:
1.) Make sure you have Node and Mongo db installed

2.) Install dependencies
### npm install

3.) Start server:
### npm run serve

4.) Make sure Mongo server is running

When server is ready you can use postman or https://editor.swagger.io/ to try it out.

[Link](https://drive.google.com/file/d/1PSlt0_n_UZuOn164jReJasQQknucG0s6/view?usp=sharing) to virtual machine with prepared development environment.





