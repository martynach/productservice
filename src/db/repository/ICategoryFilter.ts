export interface ICategoryFilter {
  categoryName?: string;
  categoryId?: string;
  sortDescendingByName?: boolean;
  sortAscendingByName?: boolean;
  projection?: Object;
}
