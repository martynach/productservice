export interface IProductFilter {
  categoryName?: string;
  categoryId?: string;
  supplierName?: string;
  supplierId?: string;
  priceFrom?: number;
  priceTo?: number;
  limit?: number;
  offset?: number;
  sortDesc?: boolean;
  projection?: Object;
}
