import { MongoError } from './MongoError';

export class ProjectionBuilder {

  public static buildProductProjection (fields: string): Object {

    const separatedFields: string[] = fields.split(',');
    const projection: Object = new Object();

    separatedFields.forEach((field: string) => {
      field = field.replace('/', '.');

      if (productProjectionFields.indexOf(field) >= 0) {
        projection[field] = 1;
      } else {
        throw new MongoError('Bad request', 400, 'Invalid field selection; /' + field + ' doest not exist');
      }
    });

    return projection;
  }

    // static buildCategoryProjection(fields: string): Object {
    //
    //     const separatedFields: Array<string> = fields.split(',');
    //     let projection = {category: {}};
    //
    //     separatedFields.forEach((field: string) => {
    //         field = field.replace('/', '.');
    //
    //         if (categoryProjectionFields.indexOf(field) > 0) {
    //             projection.category[field] = 1;
    //         } else {
    //             throw new MongoError('Bad request', 400, 'Invalid field selection; /' + field + ' doest not exist');
    //         }
    //     });
    //
    //
    //     return projection;
    // }

  public static buildCategoryProjection (fields: string): Object {

    const separatedFields: string[] = fields.split(',');
    const projection = {};

    separatedFields.forEach((field: string) => {
      field = field.replace('/', '.');

      if (categoryProjectionFields.indexOf(field) >= 0) {
        projection[field] = 1;
      } else {
        throw new MongoError('Bad request', 400, 'Invalid field selection; /' + field + ' doest not exist');
      }
    });

       // projection['_id'] = '$category._id';

    return projection;
  }

  public static buildSupplierProjection (fields: string): Object {

    const separatedFields: string[] = fields.split(',');
    const projection = {};

    separatedFields.forEach((field: string) => {
      field = field.replace('/', '.');

      if (supplierProjectionFields.indexOf(field) >= 0) {
        projection[field] = 1;
      } else {
        throw new MongoError('Bad request', 400, 'Invalid field selection; /' + field + ' doest not exist');
      }
    });

    return projection;
  }
}

const productProjectionFields = [
  '_id',
  'productName',
  'supplier',
  'supplier.supplierId',
  'supplier.companyName',
  'supplier.contacTitle',
  'supplier.address',
  'supplier.city',
  'supplier.region',
  'supplier.postalCode',
  'supplier.country',
  'supplier.phone',
  'supplier.fax',
  'supplier.homePage',
  'category',
  'category._id',
  'category.categoryName',
  'category.description',
  'category.picture',
  'quantityPerUnit',
  'unitPrice',
  'unitsInStock',
  'unitsOnOrder',
  'reorderLevel',
  'discontinued'
];

const categoryProjectionFields = [
  'categoryName',
  'description'
];

const supplierProjectionFields = [
  'companyName',
  'contacTitle',
  'address',
  'city',
  'region',
  'postalCode',
  'country',
  'phone',
  'fax',
  'homePage'
];
