export interface ISupplierFilter {
  companyName?: string;
  supplierId?: string;
  sortDescendingByName?: boolean;
  sortAscendingByName?: boolean;
  projection?: Object;
}
