import { IProductFilter } from './IProductFilter';
import * as mongoose from 'mongoose';
import { ICategoryFilter } from './ICategoryFilter';
import { ISupplierFilter } from './ISupplierFilter';

export class AggregatePipelineBuilder {
  public buildPipelineForProductFilter (productFilter: IProductFilter): Object[] {
    let pipeline = [];

    if (productFilter.priceFrom) {
      pipeline.push({ $match: { unitPrice: { $gte: productFilter.priceFrom } } });
    }

    if (productFilter.priceTo) {
      pipeline.push({ $match: { unitPrice: { $lte: productFilter.priceTo } } })
    }

    if (productFilter.categoryName) {
      pipeline.push({ $match: { 'category.categoryName': { $regex: '.*' + productFilter.categoryName.toLowerCase() + '.*' } } });
    }

    if (productFilter.categoryId) {
      pipeline.push({ $match: { 'category._id': mongoose.Types.ObjectId(productFilter.categoryId) } });
    }

    if (productFilter.supplierName) {
      pipeline.push({ $match: { 'supplier.companyName': { $regex: '.*' + productFilter.supplierName.toLowerCase() + '.*' } } });
    }

    if (productFilter.supplierId) {
      pipeline.push({ $match: { 'supplier._id': mongoose.Types.ObjectId(productFilter.supplierId) } });
    }

    if (productFilter.sortDesc) {
      pipeline.push({ $sort: { unitPrice: -1 } });
    } else {
      pipeline.push({ $sort: { unitPrice: 1 } });
    }

    if (productFilter.projection) {
      pipeline.push({ $project: productFilter.projection });
    }

    if (productFilter.offset && productFilter.offset > 0) {
      pipeline.push({ $skip: productFilter.offset });
    }

    if (productFilter.limit && productFilter.limit > 0) {
      pipeline.push({ $limit: productFilter.limit });
    }

    return pipeline;
  }

  public buildPipelineForCategoryFilter (categoryFilter: ICategoryFilter): Object[] {
    let pipeline = [];

    if (categoryFilter.categoryName) {
      pipeline.push({ $match: { 'category.categoryName': { $regex: '.*' + categoryFilter.categoryName.toLowerCase() + '.*' } } });
    }

    if (categoryFilter.categoryId) {
      pipeline.push({ $match: { 'category._id': mongoose.Types.ObjectId(categoryFilter.categoryId) } });
    }

    if (categoryFilter.sortDescendingByName) {
      pipeline.push({ $sort: { 'category.categoryName': -1 } });
    } else if (categoryFilter.sortAscendingByName) {
      pipeline.push({ $sort: { 'category.categoryName': 1 } });
    }

    pipeline.push({
      $group: {
        _id: '$category._id',
        categoryName: { '$first': '$category.categoryName' },
        description: { '$first': '$category.description' }
      }
    });

    if (categoryFilter.projection) {
      pipeline.push({ $project: categoryFilter.projection });
    }

    return pipeline;
  }

  public buildPipelineForSupplierFilter (supplierFilter: ISupplierFilter): Object[] {
    let pipeline = [];

    if (supplierFilter.companyName) {
      pipeline.push({ $match: { 'supplier.companyName': { $regex: '.*' + supplierFilter.companyName.toLowerCase() + '.*' } } });
    }

    if (supplierFilter.sortDescendingByName) {
      pipeline.push({ $sort: { 'supplier.companyName': -1 } });
    } else if (supplierFilter.sortAscendingByName) {
      pipeline.push({ $sort: { 'supplier.companyName': 1 } });
    }

    pipeline.push({
      $group: {
        _id: '$supplier._id',
        companyName: { '$first': '$supplier.companyName' },
        contactName: { '$first': '$supplier.contactName' },
        address: { '$first': '$supplier.address' },
        city: { '$first': '$supplier.city' },
        region: { '$first': '$supplier.region' },
        postalCode: { '$first': '$supplier.postalCode' },
        country: { '$first': '$supplier.country' },
        phone: { '$first': '$supplier.phone' },
        fax: { '$first': '$supplier.fax' },
        homePage: { '$first': '$supplier.homePage' }
      }
    });

    if (supplierFilter.projection) {
      pipeline.push({ $project: supplierFilter.projection });
    }

    return pipeline;
  }
}
