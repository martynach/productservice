import { IProductFilter } from './IProductFilter';
import { IProduct } from '../model/products/IProduct';
import { ICategoryFilter } from './ICategoryFilter';
import { ICategory } from '../model/products/ICategory';
import { ISupplierFilter } from './ISupplierFilter';
import { ISupplier } from '../model/products/ISupplier';

export interface IRepository {

  getProducts (productFilter: IProductFilter): Promise<IProduct[]>;
  getCategories (categoryFilter: ICategoryFilter): Promise<ICategory[]>;
  getSuppliers (supplierFilter: ISupplierFilter): Promise<ISupplier[]>;
  getProductById (productId: string): Promise<IProduct>;
  addProduct (product: IProduct): Promise<IProduct>;
  editProduct (productId: string, update: Object): Promise<IProduct>;
  deleteProduct (productId: string): Promise<IProduct>;
}
