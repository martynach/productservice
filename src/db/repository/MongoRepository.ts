import { provideSingleton, TYPES } from '../../config/inversify.config';
import { IRepository } from './IRepository';
import { Connection } from 'mongoose';
import * as mongoose from 'mongoose';
import { AggregatePipelineBuilder } from './AggregatePipelineBuilder';
import { IProduct } from '../model/products/IProduct';
import { IProductFilter } from './IProductFilter';
import { IMongooseProduct } from '../model/products/product.model';
import { ICategoryFilter } from './ICategoryFilter';
import { ICategory } from '../model/products/ICategory';
import { ISupplierFilter } from './ISupplierFilter';
import { ISupplier } from '../model/products/ISupplier';

const MongooseProduct = require('../model/products/product.model');

@provideSingleton(TYPES.IRepository)
export class MongoRepository implements IRepository {

  private db: Connection = mongoose.connection;
  private connectionString: string = process.env.MONGODB_URI || 'mongodb://localhost/Northwind';

  constructor () {
    this.db.on('error', () => {
      console.log('Connection error');
    });
    this.db.on('connected', () => {
      console.log('Connected to: ' + this.connectionString);
    });
    this.connectDB();
  }

  public async connectDB () {
    mongoose.connect(this.connectionString);
  }

  public async getProducts (productFilter: IProductFilter): Promise<IProduct[]> {

    const aggregatePipeline = new AggregatePipelineBuilder().buildPipelineForProductFilter(productFilter);
    console.log('AggregatePipelineBuilder: Built aggregate pipeline: ');
    console.log(aggregatePipeline);

    try {
      const result: IProduct[] = await MongooseProduct.aggregate(aggregatePipeline);
      console.log('MongoRepository: Retrieved products.');
      return result;
    } catch (error) {
      console.log('MongoRepository: Error while retrieving products:');
      throw error;
    }
  }

  public async getProductById (productId: string): Promise<IProduct> {
    try {
      const result: IProduct = await MongooseProduct.findById(productId).then();
      console.log('MongoRepository: Retrieved product with id: ' + productId);
      return result;
    } catch (error) {
      console.log('MongoRepository: Error while retrieving product with id: ' + productId);
      throw error;
    }
  }

  public async addProduct (product: IProduct): Promise<IProduct> {
    try {
      const mongooseProduct: IMongooseProduct = new MongooseProduct(product);
      const result: IProduct = await mongooseProduct.save();
      console.log('MongoRepository: Added new product.');
      return result;
    } catch (error) {
      console.log('MongoRepository: Error while adding new product: ');
      throw error;
    }
  }

  public async editProduct (productId: string, update: Object): Promise<IProduct> {
    try {
      const result: IProduct = MongooseProduct.findOneAndUpdate({ _id: productId }, update);
      console.log('MongoRepository: Edited product with id: ' + productId);
      return result;
    } catch (error) {
      console.log('MongoRepository: Error while editing product with id: ' + productId);
      throw error;
    }
  }

  public async deleteProduct (productId: string): Promise<IProduct> {
    try {
      const result: IProduct = MongooseProduct.findOneAndDelete({ _id: productId });
      console.log('MongoRepository: Deleted product with id: ' + productId);
      return result;
    } catch (error) {
      console.log('MongoRepository: Error while deleting product with id: ' + productId);
      throw error;
    }
  }

  async getCategories (categoryFilter: ICategoryFilter): Promise<ICategory[]> {
    const aggregatePipeline = new AggregatePipelineBuilder().buildPipelineForCategoryFilter(categoryFilter);
    console.log('AggregatePipelineBuilder: Built aggregate pipeline: ');
    console.log(aggregatePipeline);

    try {
      const result: ICategory[] = await MongooseProduct.aggregate(aggregatePipeline);
      console.log('MongoRepository: Retrieved categories.');
      return result;
    } catch (error) {
      console.log('MongoRepository: Error while retrieving categories:');
      throw error;
    }
  }

  async getSuppliers (supplierFilter: ISupplierFilter): Promise<ISupplier[]> {
    const aggregatePipeline = new AggregatePipelineBuilder().buildPipelineForSupplierFilter(supplierFilter);
    console.log('AggregatePipelineBuilder: Built aggregate pipeline: ');
    console.log(aggregatePipeline);

    try {
      const result: ISupplier[] = await MongooseProduct.aggregate(aggregatePipeline);
      console.log('MongoRepository: Retrieved suppliers.');
      return result;
    } catch (error) {
      console.log('MongoRepository: Error while retrieving suppliers:');
      throw error;
    }
  }

}
