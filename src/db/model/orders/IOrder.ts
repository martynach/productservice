import { IOrderShipInfo } from './IOrderShipInfo';
import { IOrderedProductInfo } from './IOrderedProductInfo';

export interface IOrder {
  orderDate: Date;
  shippedDate?: Date;
  shipInfo: IOrderShipInfo;
  orderedProductInfo: IOrderedProductInfo[];
}
