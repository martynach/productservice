import { Schema } from 'mongoose';

const orderShipInfoSchema: Schema = new Schema({
  shipVia: { type: String, required: true },
  freight: { type: Number, required: true },
  shipName: { type: String, required: true },
  shipAddress: { type: String, required: true },
  shipCity: { type: String, required: true },
  shipRegion: { type: String, required: true },
  shipPostalCode: { type: String, required: true },
  shipCountry: { type: String, required: true }
}
);

const orderedProductInfoSchema: Schema = new Schema(
  {
    productName: { type: String, required: true },
    categoryName: { type: String, required: true },
    description: { type: String, required: true },
    quantityPerUnit: { type: String, required: true },
    unitPrice: { type: Number, required: true },
    quantity: { type: Number, required: true },
    discount: { type: Number, required: true },
    productId: { type: Schema.Types.ObjectId, ref: 'product', required: true }
        // db ref there will hardly ever be queries using this property only when there is a complint about order it can be used
  }
);

export const orderSchema: Schema = new Schema({
  orderDate: { type: Date, required: true },
  shippedDate: { type: Date },
  shipInfo: { type: orderShipInfoSchema, required: true },
  orderedProductInfo: [orderedProductInfoSchema]
});
