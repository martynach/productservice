export interface IOrderedProductInfo {
  productName: string;
  categoryName: string;
  description: string;
  quantityPerUnit: string;
  unitPrice: number;
  quantity: number;
  discount: number;
  productId: string;
    // db ref there will hardly ever be queries using this property only when there is a complint about order it can be used
}
