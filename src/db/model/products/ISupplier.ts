export interface ISupplier {
  _id?: string;
  companyName: string;
  contacTitle: string;
  address: string;
  city: string;
  region?: string;
  postalCode: string;
  country: string;
  phone: string;
  fax?: string;
  homePage?: string
}
