import { Document, model, Model, Schema } from 'mongoose';
import { IProduct } from './IProduct';

const categorySchema = new Schema({
  categoryName: { type: String, required: true, lowercase: true, unique: true },
  description: { type: String, required: true },
  picture: { type: String }
});

const supplierSchema = new Schema({
  companyName: { type: String, required: true, lowercase: true, unique: true },
  contactName: { type: String, required: true },
  contactTitle: { type: String },
  address: { type: String, required: true },
  city: { type: String, required: true },
  region: { type: String },
  postalCode: { type: String, required: true },
  country: { type: String, required: true },
  phone: { type: String, required: true },
  fax: { type: String },
  homePage: { type: String }
});

const productSchema: Schema = new Schema({
  productName: { type: String, required: true, lowercase: true, unique: true },
  supplier: { type: supplierSchema, required: true },
  category: { type: categorySchema, required: true },
  quantityPerUnit: { type: String, required: true },
  unitPrice: { type: Number, required: true },
  unitsInStock: { type: Number, required: true },
  unitsOnOrder: { type: Number, required: true },
  reorderLevel: { type: Number },
  discontinued: { type: Number }
});

export interface IMongooseProduct extends IProduct, Document {
}

const MongooseProduct: Model<IMongooseProduct> = model<IMongooseProduct>('product', productSchema);

module.exports = MongooseProduct;
