import { ISupplier } from './ISupplier';
import { ICategory } from './ICategory';

export interface IProduct {
  productName: string;
  supplier: ISupplier;
  category: ICategory;
  quantityPerUnit: string,
  unitPrice: number,
  unitsInStock: number,
  unitsOnOrder: number,
  reorderLevel?: number,
  discontinued?: boolean
}
