import { IOrder } from '../orders/IOrder';

export interface IEmployee {
  employeeId?: string;
  lastName: string;
  firstName: string;
  title: string;
  titleOfCourtesy?: string;
  birthDate: Date;
  hireDate: Date;
  address: string;
  city: string;
  region: string;
  postalCode: string;
  country: string;
  homePhone: string;
  extension?: string;
  photo?: string;
  notes?: string;
  reportsTo: string; // db ref
  photoPath?: string;
  orders: IOrder[];
}
