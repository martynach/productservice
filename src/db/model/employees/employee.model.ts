import { Document, model, Model, Schema } from 'mongoose';
import { orderSchema } from '../orders/order.model';
import { ICustomer } from '../customers/ICustomer';

const employeeSchema: Schema = new Schema({
  lastName: { type: String, required: true },
  firstName: { type: String, required: true },
  title: { type: String, required: true },
  titleOfCourtesy: { type: String, required: true },
  birthDate: { type: Date, required: true },
  hireDate: { type: Date, required: true },
  address: { type: String, required: true },
  city: { type: String, required: true },
  region: { type: String, required: true },
  postalCode: { type: String, required: true },
  country: { type: String, required: true },
  homePhone: { type: String, required: true },
  extension: { type: String },
  photo: { type: String },
  notes: { type: String },
  reportsTo: { type: Schema.Types.ObjectId, ref: 'employee', required: true },
  photoPath: { type: String },
  orders: [orderSchema]
});

export interface IMongooseEmployee extends ICustomer, Document {
}

const MongooseEmployee: Model<IMongooseEmployee> = model<IMongooseEmployee>('employee', employeeSchema);
module.exports = MongooseEmployee;
