import { IOrder } from '../orders/IOrder';

export interface ICustomer {
  customerId?: string;
  companyName: string,
  contactName: string;
  contactTitle: string;
  address: string;
  city: string;
  region: string;
  postalCode: string;
  country: string;
  phone?: string;
  fax?: string;
  orders: IOrder[];
}
