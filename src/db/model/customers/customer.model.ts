import { Document, model, Model, Schema } from 'mongoose';
import { orderSchema } from '../orders/order.model';
import { ICustomer } from './ICustomer';

const customerSchema: Schema = new Schema({
  companyName: { type: String, required: true, lowercase: true, unique: true },
  contactName: { type: String, required: true },
  contactTitle: { type: String, required: true },
  address: { type: String, required: true },
  city: { type: String, required: true },
  region: { type: String, required: true },
  postalCode: { type: String, required: true },
  country: { type: String, required: true },
  phone: { type: String },
  fax: { type: String },
  orders: [orderSchema]
});

export interface IMongooseCustomer extends ICustomer, Document {
}

const MongooseCustomer: Model<IMongooseCustomer> = model<IMongooseCustomer>('customer', customerSchema);

module.exports = MongooseCustomer;
