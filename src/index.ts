import * as http from 'http';
import app from './app';
import './controllers/category.controller';
import './controllers/product.controller';
import './controllers/supplier.controller';

const port: Number = process.env.PORT ? Number(process.env.PORT) : 3020;

const server = http.createServer(app);
server.listen(port);

server.on('error', onError);
server.on('listening', onListening);

function onError (error: NodeJS.ErrnoException): void {
  throw error;
}

function onListening (): void {
  console.log('Listening on port ' + port);
}
