// @ts-ignore
import { Get, Route, Controller, Query } from 'tsoa';
import { IRepository } from '../db/repository/IRepository';
import { provideSingleton, TYPES } from '../config/inversify.config';
import { inject } from 'inversify';
import { ProjectionBuilder } from '../db/repository/ProjectionBuilder';
import { ISupplierFilter } from '../db/repository/ISupplierFilter';
import { ISupplier } from '../db/model/products/ISupplier';

@Route('suppliers')
@provideSingleton(SupplierController)
export class SupplierController extends Controller {

  @inject(TYPES.IRepository) private repository: IRepository;

  @Get()
    public async getSuppliers (@Query() companyName?: string,
                             @Query() supplierId?: string,
                             @Query() sortDescendingByName?: boolean,
                             @Query() sortAscendingByName?: boolean,
                             @Query() fields?: string): Promise<ISupplier[]> {

    const projection: Object = fields ? ProjectionBuilder.buildSupplierProjection(fields) : undefined;

    console.log('supplier controller2');
    console.log('sortDescendingByName: ' + sortDescendingByName);
    console.log('sortAscendingByName: ' + sortAscendingByName);
    console.log('******************');

    const supplierFilter: ISupplierFilter = {
      companyName: companyName,
      supplierId: supplierId,
      sortDescendingByName: sortDescendingByName,
      sortAscendingByName: sortAscendingByName,
      projection: projection
    };

    return this.repository.getSuppliers(supplierFilter);
  }

}
