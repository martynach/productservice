// @ts-ignore
import { Get, Route, Controller, Query, Post, Body, Put, Delete } from 'tsoa';
import { IRepository } from '../db/repository/IRepository';
import { provideSingleton, TYPES } from '../config/inversify.config';
import { inject } from 'inversify';
import { IProductFilter } from '../db/repository/IProductFilter';
import { IProduct } from '../db/model/products/IProduct';
import { ProjectionBuilder } from '../db/repository/ProjectionBuilder';

@Route('products')
@provideSingleton(ProductController)
export class ProductController extends Controller {

  @inject(TYPES.IRepository) private repository: IRepository;

    // todo add to wiki info about implementing partial response
    // https://developers.google.com/drive/api/v3/performance?fbclid=IwAR1YOoXpZaWWY7-3xen-DzuFHakcycR1VxXRJRvAwOjb5nwjL93SvVN9KqI#partial

  @Get()
    public async getProducts (@Query() categoryName?: string,
                             @Query() categoryId?: string,
                             @Query() supplierName?: string,
                             @Query() supplierId?: string,
                             @Query() priceFrom?: number,
                             @Query() priceTo?: number,
                             @Query() limit?: number,
                             @Query() offset?: number,
                             @Query() sortDesc?: boolean,
                             @Query() fields?: string): Promise<IProduct[]> {

    const projection: Object = fields ? ProjectionBuilder.buildProductProjection(fields) : undefined;

    const productFilter: IProductFilter = {
      categoryName: categoryName,
      categoryId: categoryId,
      supplierName: supplierName,
      supplierId: supplierId,
      priceFrom: priceFrom,
      priceTo: priceTo,
      limit: limit,
      offset: offset,
      sortDesc: sortDesc,
      projection: projection
    };

    return this.repository.getProducts(productFilter);
  }

  @Get('{productId}')
    public async getProduct (productId: string): Promise<IProduct> {
    return this.repository.getProductById(productId);
  }

  @Post()
    public async addProduct (@Body() req): Promise<IProduct> {
    return this.repository.addProduct(req);
  }

  @Post('{productId}')
    public async editProduct (productId: string, @Body() update): Promise<IProduct> {
    return this.repository.editProduct(productId, update);
  }

  @Delete('{productId}')
    public async deleteProduct (productId: string): Promise<IProduct> {
    return this.repository.deleteProduct(productId);
  }

}
