// @ts-ignore
import { Get, Route, Controller, Query, Post, Body, Put, Delete } from 'tsoa';
import { IRepository } from '../db/repository/IRepository';
import { provideSingleton, TYPES } from '../config/inversify.config';
import { inject } from 'inversify';
import { ProjectionBuilder } from '../db/repository/ProjectionBuilder';
import { ICategoryFilter } from '../db/repository/ICategoryFilter';
import { ICategory } from '../db/model/products/ICategory';

@Route('categories')
@provideSingleton(CategoryController)
export class CategoryController extends Controller {

  @inject(TYPES.IRepository) private repository: IRepository;

  @Get()
    public async getCategories (@Query() categoryName?: string,
                             @Query() categoryId?: string,
                             @Query() sortDescendingByName?: boolean,
                             @Query() sortAscendingByName?: boolean,
                             @Query() fields?: string): Promise<ICategory[]> {

    console.log('fields: ' + fields);
    const projection: Object = fields ? ProjectionBuilder.buildCategoryProjection(fields) : undefined;

    const categoryFilter: ICategoryFilter = {
      categoryName: categoryName,
      categoryId: categoryId,
      sortDescendingByName: sortDescendingByName,
      sortAscendingByName: sortAscendingByName,
      projection: projection
    };

    return this.repository.getCategories(categoryFilter);
  }

}
