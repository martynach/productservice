/* tslint:disable */
import { Controller, ValidationService, FieldErrors, ValidateError, TsoaRoute } from 'tsoa';
import { iocContainer } from './../config/inversify.config';
import { ProductController } from './../controllers/product.controller';
import { CategoryController } from './../controllers/category.controller';
import { SupplierController } from './../controllers/supplier.controller';
import * as express from 'express';

const models: TsoaRoute.Models = {
    "ISupplier": {
        "properties": {
            "_id": { "dataType": "string" },
            "companyName": { "dataType": "string", "required": true },
            "contacTitle": { "dataType": "string", "required": true },
            "address": { "dataType": "string", "required": true },
            "city": { "dataType": "string", "required": true },
            "region": { "dataType": "string" },
            "postalCode": { "dataType": "string", "required": true },
            "country": { "dataType": "string", "required": true },
            "phone": { "dataType": "string", "required": true },
            "fax": { "dataType": "string" },
            "homePage": { "dataType": "string" },
        },
    },
    "ICategory": {
        "properties": {
            "_id": { "dataType": "string" },
            "categoryName": { "dataType": "string", "required": true },
            "description": { "dataType": "string", "required": true },
            "picture": { "dataType": "string" },
        },
    },
    "IProduct": {
        "properties": {
            "productName": { "dataType": "string", "required": true },
            "supplier": { "ref": "ISupplier", "required": true },
            "category": { "ref": "ICategory", "required": true },
            "quantityPerUnit": { "dataType": "string", "required": true },
            "unitPrice": { "dataType": "double", "required": true },
            "unitsInStock": { "dataType": "double", "required": true },
            "unitsOnOrder": { "dataType": "double", "required": true },
            "reorderLevel": { "dataType": "double" },
            "discontinued": { "dataType": "boolean" },
        },
    },
};
const validationService = new ValidationService(models);

export function RegisterRoutes(app: express.Express) {
    app.get('/products',
        function(request: any, response: any, next: any) {
            const args = {
                categoryName: { "in": "query", "name": "categoryName", "dataType": "string" },
                categoryId: { "in": "query", "name": "categoryId", "dataType": "string" },
                supplierName: { "in": "query", "name": "supplierName", "dataType": "string" },
                supplierId: { "in": "query", "name": "supplierId", "dataType": "string" },
                priceFrom: { "in": "query", "name": "priceFrom", "dataType": "double" },
                priceTo: { "in": "query", "name": "priceTo", "dataType": "double" },
                limit: { "in": "query", "name": "limit", "dataType": "double" },
                offset: { "in": "query", "name": "offset", "dataType": "double" },
                sortDesc: { "in": "query", "name": "sortDesc", "dataType": "boolean" },
                fields: { "in": "query", "name": "fields", "dataType": "string" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = iocContainer.get<ProductController>(ProductController);
            if (typeof controller['setStatus'] === 'function') {
                (<any>controller).setStatus(undefined);
            }


            const promise = controller.getProducts.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.get('/products/:productId',
        function(request: any, response: any, next: any) {
            const args = {
                productId: { "in": "path", "name": "productId", "required": true, "dataType": "string" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = iocContainer.get<ProductController>(ProductController);
            if (typeof controller['setStatus'] === 'function') {
                (<any>controller).setStatus(undefined);
            }


            const promise = controller.getProduct.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.post('/products',
        function(request: any, response: any, next: any) {
            const args = {
                req: { "in": "body", "name": "req", "required": true, "dataType": "any" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = iocContainer.get<ProductController>(ProductController);
            if (typeof controller['setStatus'] === 'function') {
                (<any>controller).setStatus(undefined);
            }


            const promise = controller.addProduct.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.post('/products/:productId',
        function(request: any, response: any, next: any) {
            const args = {
                productId: { "in": "path", "name": "productId", "required": true, "dataType": "string" },
                update: { "in": "body", "name": "update", "required": true, "dataType": "any" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = iocContainer.get<ProductController>(ProductController);
            if (typeof controller['setStatus'] === 'function') {
                (<any>controller).setStatus(undefined);
            }


            const promise = controller.editProduct.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.delete('/products/:productId',
        function(request: any, response: any, next: any) {
            const args = {
                productId: { "in": "path", "name": "productId", "required": true, "dataType": "string" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = iocContainer.get<ProductController>(ProductController);
            if (typeof controller['setStatus'] === 'function') {
                (<any>controller).setStatus(undefined);
            }


            const promise = controller.deleteProduct.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.get('/categories',
        function(request: any, response: any, next: any) {
            const args = {
                categoryName: { "in": "query", "name": "categoryName", "dataType": "string" },
                categoryId: { "in": "query", "name": "categoryId", "dataType": "string" },
                sortDescendingByName: { "in": "query", "name": "sortDescendingByName", "dataType": "boolean" },
                sortAscendingByName: { "in": "query", "name": "sortAscendingByName", "dataType": "boolean" },
                fields: { "in": "query", "name": "fields", "dataType": "string" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = iocContainer.get<CategoryController>(CategoryController);
            if (typeof controller['setStatus'] === 'function') {
                (<any>controller).setStatus(undefined);
            }


            const promise = controller.getCategories.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });
    app.get('/suppliers',
        function(request: any, response: any, next: any) {
            const args = {
                companyName: { "in": "query", "name": "companyName", "dataType": "string" },
                supplierId: { "in": "query", "name": "supplierId", "dataType": "string" },
                sortDescendingByName: { "in": "query", "name": "sortDescendingByName", "dataType": "boolean" },
                sortAscendingByName: { "in": "query", "name": "sortAscendingByName", "dataType": "boolean" },
                fields: { "in": "query", "name": "fields", "dataType": "string" },
            };

            let validatedArgs: any[] = [];
            try {
                validatedArgs = getValidatedArgs(args, request);
            } catch (err) {
                return next(err);
            }

            const controller = iocContainer.get<SupplierController>(SupplierController);
            if (typeof controller['setStatus'] === 'function') {
                (<any>controller).setStatus(undefined);
            }


            const promise = controller.getSuppliers.apply(controller, validatedArgs);
            promiseHandler(controller, promise, response, next);
        });


    function isController(object: any): object is Controller {
        return 'getHeaders' in object && 'getStatus' in object && 'setStatus' in object;
    }

    function promiseHandler(controllerObj: any, promise: any, response: any, next: any) {
        return Promise.resolve(promise)
            .then((data: any) => {
                let statusCode;
                if (isController(controllerObj)) {
                    const headers = controllerObj.getHeaders();
                    Object.keys(headers).forEach((name: string) => {
                        response.set(name, headers[name]);
                    });

                    statusCode = controllerObj.getStatus();
                }

                if (data || data === false) { // === false allows boolean result
                    response.status(statusCode || 200).json(data);
                } else {
                    response.status(statusCode || 204).end();
                }
            })
            .catch((error: any) => next(error));
    }

    function getValidatedArgs(args: any, request: any): any[] {
        const fieldErrors: FieldErrors = {};
        const values = Object.keys(args).map((key) => {
            const name = args[key].name;
            switch (args[key].in) {
                case 'request':
                    return request;
                case 'query':
                    return validationService.ValidateParam(args[key], request.query[name], name, fieldErrors);
                case 'path':
                    return validationService.ValidateParam(args[key], request.params[name], name, fieldErrors);
                case 'header':
                    return validationService.ValidateParam(args[key], request.header(name), name, fieldErrors);
                case 'body':
                    return validationService.ValidateParam(args[key], request.body, name, fieldErrors, name + '.');
                case 'body-prop':
                    return validationService.ValidateParam(args[key], request.body[name], name, fieldErrors, 'body.');
            }
        });
        if (Object.keys(fieldErrors).length > 0) {
            throw new ValidateError(fieldErrors, '');
        }
        return values;
    }
}
