import { IRepository } from '../db/repository/IRepository';
import { Container, interfaces } from 'inversify';
import { autoProvide, fluentProvide } from 'inversify-binding-decorators';
import 'reflect-metadata';

const TYPES = {
  IRepository: Symbol.for('IRepository_old')
}

const iocContainer = new Container();

const provideSingleton = function (
    identifier: string | symbol | interfaces.Newable<any> | interfaces.Abstract<any>
) {
  return fluentProvide(identifier)
        .inSingletonScope()
        .done();
};

export { iocContainer, autoProvide, provideSingleton, TYPES };
